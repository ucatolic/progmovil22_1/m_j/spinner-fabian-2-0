package com.sigmotoa.resistorcalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val spn1:Spinner=findViewById(R.id.spinner_1)
        val values = resources.getStringArray(R.array.values)

        val adaptador =ArrayAdapter(this, android.R.layout.simple_spinner_item, values)

        spn1.adapter = adaptador

        spn1.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(p2 == 0) {

                    Toast.makeText(this@MainActivity, p2.toString(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }

    }
}